import 'package:flutter/material.dart';

class Assignments extends StatelessWidget{
  const Assignments({super.key});

@override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title: const Text("Basic Assignments "),
      ),
      body:SizedBox(
        height:double.infinity,
        child:Row(
          mainAxisAlignment:MainAxisAlignment.spaceAround,
          children: [
            Container(
              height:100,width:100,
              color: Colors.brown,
            ),

            Container(
              height:100,width:100,
              color:Colors.teal,
            ),

            Container(
              height:100,width:100,
              color: Colors.redAccent,
            ),
            
          ],
        ),
      ),
    );
  }
}





