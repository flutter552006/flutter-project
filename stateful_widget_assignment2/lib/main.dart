import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment2(),
      
    );
  }
}

class Assignment2 extends StatefulWidget{
  const Assignment2({super.key});

  @override
  State<Assignment2> createState() => _Assignment2State();
}
class _Assignment2State extends State<Assignment2>{
  bool box1Color = false;
  bool box2Color = false;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text('Color Box'),
        backgroundColor:Colors.blue,
      ),
      body:Column(
        mainAxisAlignment:MainAxisAlignment.center,
        children:[
          Row(
            mainAxisAlignment:MainAxisAlignment.center,
            //crossAxisAlignment:CrossAxisAlignment.center,
            children: [
              //BOX 1 
              Column(
                children: [
                  Container(
                    height: 100,
                    width:100,
                    color:Colors.red,
                  ),
                  const SizedBox(
                    height:20,
                  ),

                  //BOX 1 BUTTON
                  ElevatedButton(
                    onPressed:(){
                      setState(() {
                        box1Color = true;
                      });
                    } ,
                    child: const Text("Color Box1"),
                  ),
                ]), 
                const SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Container(
                      height: 100,
                      width: 100,
                      color: Colors.amber,
                    ),
                    const SizedBox(
                      height: 20,
                    ),

                    //BOX 2 BUTTON
                    ElevatedButton(
                      onPressed: (){
                        // setState((){
                        //   box2Color = true;
                        // });
                      }, 
                      child: const Text("Color Box 2"),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                  ],
                ),
                const SizedBox(
                  width: 20,
                ),
            ]),
        ]),
    );
  }
}