import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment3(),
    );
  }
}
class Assignment3 extends StatefulWidget{
  const Assignment3({super.key});

  @override
 State<Assignment3> createState()=> _Assignment3State();
}
class _Assignment3State extends State<Assignment3>{
  int? selectedIndex =0;
  final List<String>imageList = [
    "https://images.unsplash.com/photo-1575550959106-5a7defe28b56?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8d2lsZGxpZmV8ZW58MHx8MHx8fDA%3D",
    
    "https://c02.purpledshub.com/uploads/sites/62/2021/09/Silhouetted-elephants.-Chris-Packham.-Remembering-Elephants-da11c3e.jpg?w=1029&webp=1",
    
    "https://media.istockphoto.com/id/1170804921/photo/turtle-closeup-with-school-of-fish.jpg?s=612x612&w=0&k=20&c=0l3Sw_Lx-9PVHjR963pvt9A6-p7sxwMe-xm9LnwxAgw=",
    
    "https://www.shutterstock.com/image-vector/african-wildlife-elephants-under-sunset-600nw-2260462777.jpg",
  ];

  void showNextImage(){
    setState(() {
      selectedIndex = selectedIndex! + 1;
    });
  }

  @override
  Widget build (BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title:const Text(" Display Image "),
      ),
      body: Center(
        child:Column(
          children: [
            Image.network(
              imageList[selectedIndex!],
              width: 500,
              height: 500,
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed:showNextImage,
              child:const Text("Next")
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: (){
                setState((){
                  selectedIndex =0;

                });
              }, 
              child: const Text('reset')
            ),
          ],
        )),
    );
  }
}
