import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: TextFieldDemo(),
      debugShowCheckedModeBanner:false,

    );
  }
}

class TextFieldDemo extends StatefulWidget{

  const TextFieldDemo({super.key});

  @override
  State createState()=> _TextFieldDemoState();
}

class _TextFieldDemoState extends State{

  TextEditingController nameController = TextEditingController();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("TextFieldDemoFile"),
        centerTitle:true,
        backgroundColor: Colors.blue,
      ),

      body: const Column(
        children: [
          SizedBox(
            height: 30,
          ),
          TextField(
           // showCursor: false,
            decoration:InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(30),
                ),
              ),
              hintText: "Enter Comapany Name", hintStyle: TextStyle(fontSize: 20),
            ),
            keyboardType: TextInputType.emailAddress,
          ),
          SizedBox(
            height:30
          ),
          TextField(
          ),
        ],
      ),
    );
  }
}