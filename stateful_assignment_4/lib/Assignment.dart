import 'package:flutter/material.dart';

class Assignment extends StatefulWidget{
  const Assignment({super.key});

  @override
  State<Assignment> createState() => _AssignmentState();
}
class _AssignmentState extends State<Assignment>{
  int count =0;
  void countPalidrome(){
    count = 0;
    List<int> numberList = [1,225,-777,121,111,234,999];
    for(int i=0;i<numberList.length;i++){
      int temp = numberList[i].abs();
      int reverseNum = 0;
      while(temp!=0){
        reverseNum = reverseNum * 10 + temp%10;
        temp = temp ~/ 10;
      }
      if(reverseNum == numberList[i].abs()){
        count++;
      }
    }
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text( "Assignment 4" ),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: (){
                countPalidrome();
                setState(() {});
              }, 
            child: const Text("Check palindrome"),
            ),
            const SizedBox(
              height: 20,
            ),
            Text("$count Number are Palindrome "),
           
          ],),
      ),
    );
  }
}
