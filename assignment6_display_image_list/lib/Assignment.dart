import 'package:flutter/material.dart';

class Assignment extends StatefulWidget{
  const Assignment({super.key});

@override
State <Assignment> createState() => _AssignmentState();
}

class _AssignmentState extends State<Assignment>{

  // VARIABLE
  int? selectedIndex = 0;

  // LIST OF IMAGE  
  final List<String> imageList = [
    "https://buffer.com/library/content/images/size/w1200/2023/10/free-images.jpg",

    "https://buffer.com/library/content/images/size/w1200/2023/10/free-images.jpg",

    "https://buffer.com/library/content/images/size/w1200/2023/10/free-images.jpg",

    //"https://buffer.com/library/content/images/size/w1200/2023/10/free-images.jpg",
  ];
  void showNextImage(){
    setState(() {
      if(selectedIndex < imageList.length -1 ){
        selectedIndex = selectedIndex + 1;
      }else{
        selectedIndex = 0; // Reset to first image
      }
      //selectedIndex = selectedIndex! +1;
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      //backgroundColor: Colors.green,
      appBar: AppBar(
        title: const Text(
          "Display Image List"
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment : MainAxisAlignment.center,
            children: [
                Image.network(
                  imageList[selectedIndex!],
                width: 300,
                height: 300,
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: showNextImage, 
                  child: const Text("Next"),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ElevatedButton(
                  onPressed:(){
                    setState(() {
                      selectedIndex = 0;
                    });
                  },
                  child: const Text("Rest"),
                )   
            ],
          ),
        ),
      );
  }
}
