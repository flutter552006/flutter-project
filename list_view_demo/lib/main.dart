import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner:false,
      home: Scaffold(
        body: ListViewDemo(),
      ),
    );
  }
}

class ListViewDemo extends StatefulWidget{
  const ListViewDemo({super.key});

  @override
  State createState() => _ListViewDemoState();
}

class _ListViewDemoState extends State{
  List imagesList = [
    "https://th.bing.com/th/id/OIP.S_Kwi8mM1_gTrWDBkF__MgHaLH?w=216&h=324&c=7&o=5&dpr=1.3&pid=1.7",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuhiHK1e6Bd9WUtNkIL046utdp1GiSDYVOMWXDjYSN6xIUFHkoKIfmUrgf5BmIy3iLero&usqp=CAU",
    "https://th.bing.com/th/id/OIP.S_Kwi8mM1_gTrWDBkF__MgHaLH?w=216&h=324&c=7&o=5&dpr=1.3&pid=1.7",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuhiHK1e6Bd9WUtNkIL046utdp1GiSDYVOMWXDjYSN6xIUFHkoKIfmUrgf5BmIy3iLero&usqp=CAU",
    "https://th.bing.com/th/id/OIP.S_Kwi8mM1_gTrWDBkF__MgHaLH?w=216&h=324&c=7&o=5&dpr=1.3&pid=1.7",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuhiHK1e6Bd9WUtNkIL046utdp1GiSDYVOMWXDjYSN6xIUFHkoKIfmUrgf5BmIy3iLero&usqp=CAU",
    "https://th.bing.com/th/id/OIP.S_Kwi8mM1_gTrWDBkF__MgHaLH?w=216&h=324&c=7&o=5&dpr=1.3&pid=1.7",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuhiHK1e6Bd9WUtNkIL046utdp1GiSDYVOMWXDjYSN6xIUFHkoKIfmUrgf5BmIy3iLero&usqp=CAU",
    "https://th.bing.com/th/id/OIP.S_Kwi8mM1_gTrWDBkF__MgHaLH?w=216&h=324&c=7&o=5&dpr=1.3&pid=1.7",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuhiHK1e6Bd9WUtNkIL046utdp1GiSDYVOMWXDjYSN6xIUFHkoKIfmUrgf5BmIy3iLero&usqp=CAU",
    "https://th.bing.com/th/id/OIP.S_Kwi8mM1_gTrWDBkF__MgHaLH?w=216&h=324&c=7&o=5&dpr=1.3&pid=1.7",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuhiHK1e6Bd9WUtNkIL046utdp1GiSDYVOMWXDjYSN6xIUFHkoKIfmUrgf5BmIy3iLero&usqp=CAU"

  ];

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        
        title: const Text("ListViewDemo"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),

/// 1st 
      // body:ListView(
      //   children: [
      //     Container(
      //       height: 300,
      //       width: 100,
      //       margin: const EdgeInsets.all(10),
      //       child: Image.network("https://images.news18.com/ibnlive/uploads/2023/03/virat-kohli-took-the-helmet-off-and-soaked-it-all-in-ap-photo.jpg"),
      //     ),
      //     Container(
      //       height: 300,
      //       width: 100,
      //       margin: EdgeInsets.all(10),
      //       child: Image.network("https://images.news18.com/ibnlive/uploads/2023/03/virat-kohli-took-the-helmet-off-and-soaked-it-all-in-ap-photo.jpg"),
      //     ),
      //     Container(
      //       height: 300,
      //       width: 100,
      //       margin: EdgeInsets.all(10),
      //       child: Image.network("https://images.news18.com/ibnlive/uploads/2023/03/virat-kohli-took-the-helmet-off-and-soaked-it-all-in-ap-photo.jpg"),
      //     ),
      //     Container(
      //       height: 300,
      //       width: 100,
      //       margin: EdgeInsets.all(10),
      //       child: Image.network("https://images.news18.com/ibnlive/uploads/2023/03/virat-kohli-took-the-helmet-off-and-soaked-it-all-in-ap-photo.jpg"),
      //     ),
      //   ],
      // )
  

/// 2nd
    // body: ListView.builder(
    //     itemCount: imagesList.length,
    //     itemBuilder:(BuildContext contex,int index){
    //       return Container(
    //         height: 200,
    //         width: 200,
    //         margin: const EdgeInsets.all(10),
    //         child: Image.network(imagesList[index]),
    //       );
    //     }
    //   ),


/// 3rd
    body: ListView.separated(
        itemCount: imagesList.length,
        separatorBuilder: (BuildContext context , int index){
          return const Text("==================================================================");
            // If you return function semicolon use  not comma. 
        },
        itemBuilder:(BuildContext contex,int index){
          return Container(
            height: 500,
            width: 400,
            margin: const EdgeInsets.all(10),
            child: Image.network(imagesList[index],
              
            ),
          );
        }
      ),
    );
  }
}

