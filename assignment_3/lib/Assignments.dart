import 'package:flutter/material.dart';

 class Assignments extends StatefulWidget{
  const Assignments({super.key});

  State<Assignments> createState() => _AssignmentsState();
}
class _AssignmentsState extends State<Assignments>{


@override
  Widget build(BuildContext context)=> Scaffold(
      appBar:AppBar(
      backgroundColor: Colors.brown,
        title:const Text("Assignments 3",
        ),
      ),
      body: SizedBox(
        height: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,width: 100,
                  color: Colors.amber,
                ),
                SizedBox(
                  height: 20,
                ),
                
                ElevatedButton(onPressed: () {}, 
                child: const Text(" Button 1 ")
                )
              ],
            ),
            Column(
              mainAxisAlignment:MainAxisAlignment.center,
              children: [
                Container(
                  height: 100, width: 100,
                  color:Colors.green,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(onPressed: () {}, 
                  child: const Text(" Button 2")N
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children : [
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.lightBlueAccent,
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(onPressed: (){},
                 child: const Text(" Button 3")
                 ),
              ]
            )
          ],
        ),
      ),
    );
  }

